﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.Infrastucture.EntityFramework.Extensions
{
    public static class PagedExtensions
    {
        public static IEnumerable<T> GetPaged<T>(this IQueryable<T> query,
                                         int page, int pageSize) where T : class
        {
            var skip = (page - 1) * pageSize;
            return query.Skip(skip).Take(pageSize).ToList();
        }
    }
}
