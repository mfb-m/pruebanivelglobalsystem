export class ResponseBase<T>
{
    count : number
    response : T
    errors : Array<string>
    isValid : boolean
    total? : number
}