import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarShellComponent } from './components/navbar-shell/navbar-shell.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavbarShellComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    NavbarShellComponent
  ]
})
export class ShellModule { }
