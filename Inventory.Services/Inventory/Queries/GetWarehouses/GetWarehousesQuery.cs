﻿using Inventory.Domain.Entities;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Queries.GetWarehouses
{
    public class GetWarehousesQuery : IQuery<ResponseBase<IEnumerable<Warehouse>>>
    {
    }
}
