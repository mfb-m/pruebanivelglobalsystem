import { TestBed } from '@angular/core/testing';

import { InventoryWebshocketsService } from './inventory-webshockets.service';

describe('InventoryWebshocketsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InventoryWebshocketsService = TestBed.get(InventoryWebshocketsService);
    expect(service).toBeTruthy();
  });
});
