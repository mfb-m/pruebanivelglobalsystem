import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { FetchGetWarehouses, FetchAddProduct } from '../../store/actions/user.actions';
import { Observable } from 'rxjs';
import { IWarehouse } from 'src/app/models/Warehouse';
import { AppState } from 'src/app/app.state';
import * as M from 'materialize-css/dist/js/materialize';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EntryType } from 'src/app/models/WarehouseLedgerEntry';
import { TypeProduct } from 'src/app/models/Product';
import { IAddProductCommand } from 'src/app/models/AddProductCommand';
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    M.AutoInit();
  }

  addProductForm = new FormGroup({
    entryType: new FormControl('', Validators.required),
    productType: new FormControl('', Validators.required),
    warehouse : new FormControl('', Validators.required),
    expireDate : new FormControl('', Validators.required),
    productName : new FormControl('', Validators.required),
    productDescription : new FormControl('', Validators.required),
    imageUrl : new FormControl('', Validators.required),
    purchasePrice : new FormControl('', Validators.required),
    weight : new FormControl('', Validators.required),
    salesPrice : new FormControl('', Validators.required),
    quantity : new FormControl('', Validators.required)
  });


  dataWarehouses$: Observable<IWarehouse[]>;
  dataWarehouses: IWarehouse[];
  entryTypes = EntryType
  productTypes = TypeProduct

  @ViewChild('selectWarehouses') selectWarehouses : ElementRef
  constructor(private store : Store<AppState>) {

   }

  ngOnInit() {
    this.store.select(o => o.inventory.warehouse).subscribe(o => this.dataWarehouses = o)
    this.store.dispatch(new FetchGetWarehouses())
  }


  saveProduct(){
    let addProductCommand : IAddProductCommand = {
      entryId : uuid(),
      dateAdded : new Date(),
      description : this.addProductForm.controls.productDescription.value,
      entryType : this.addProductForm.controls.entryType.value,
      expireDate : this.addProductForm.controls.expireDate.value,
      imageUrl : this.addProductForm.controls.imageUrl.value,
      name : this.addProductForm.controls.productName.value,
      productId : uuid(),
      purchasePrice : this.addProductForm.controls.purchasePrice.value,
      quantity : this.addProductForm.controls.quantity.value,
      salesPrice : this.addProductForm.controls.salesPrice.value,
      type : this.addProductForm.controls.productType.value,
      warehouseId : this.addProductForm.controls.warehouse.value,
      weight : this.addProductForm.controls.weight.value
    }
    this.store.dispatch(new FetchAddProduct(addProductCommand))
    this.addProductForm.reset()
  }
}
