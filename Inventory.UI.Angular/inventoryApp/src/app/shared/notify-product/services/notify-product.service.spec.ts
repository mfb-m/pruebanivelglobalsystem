import { TestBed } from '@angular/core/testing';

import { NotifyProductService } from './notify-product.service';

describe('NotifyProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotifyProductService = TestBed.get(NotifyProductService);
    expect(service).toBeTruthy();
  });
});
