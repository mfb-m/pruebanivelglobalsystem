﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Shared
{
    public class ResponseBase<T>
    {
        public int Count { get; set; }
        public T Response { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public bool IsValid { get; set; }
        public int Total { get; set; }
    }
}
