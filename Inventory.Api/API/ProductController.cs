﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Api.API.Shared;
using Inventory.Infrastructure.Bus.Commands;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Inventory.Commands.AddProduct;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Api.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : InventoryControllerBase
    {
        public ProductController(IQueryBus queryBus, ICommandBus commandBus) : base(queryBus, commandBus)
        {
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] AddProductCommand addProductCommand)
        {
            await _commandBus.Send(addProductCommand);
            return Ok();

        }
    }
}
