import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseLedgerEntryTableComponent } from './warehouse-ledger-entry-table.component';

describe('WarehouseLedgerEntryTableComponent', () => {
  let component: WarehouseLedgerEntryTableComponent;
  let fixture: ComponentFixture<WarehouseLedgerEntryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseLedgerEntryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseLedgerEntryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
