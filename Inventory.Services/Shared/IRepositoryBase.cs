﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Shared
{
    public interface IRepositoryBase
    {
        bool SaveChanges();
    }
}
