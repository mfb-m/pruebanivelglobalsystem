import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from } from 'rxjs';
import { switchMap, catchError, tap } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { ConnectWebshockets, AppActionsTypes, ConnectWebshocketsSucess, ConnectWebshocketsFail, ShowNotificationProductAdded, ShowNotificationProductExpired, ShowNotificationProductRemoved } from '../actions/app.actions';
import { InventoryWebshocketsService } from '../../services/inventory-webshockets.service';
import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';
import { NotifyProductService } from 'src/app/shared/notify-product/services/notify-product.service';

@Injectable()
  export class AppEffects {
  
    constructor(
        private actions$: Actions, 
        private inventoryWebshocketsService : InventoryWebshocketsService,
        private notifyProductService : NotifyProductService) { }


    @Effect()
    connectWebshockets$ = this.actions$.pipe(
      ofType<ConnectWebshockets>(AppActionsTypes.ConnectWebshockets),
      switchMap(() => {
        return from(this.inventoryWebshocketsService.initConnection())
      }),
      switchMap(()=> [
          new ConnectWebshocketsSucess()
      ]),
      catchError((error) => {
          return of(new ConnectWebshocketsFail(error))
      })

    );

    @Effect({dispatch : false})
    ShowNotificationProductAdded$ = this.actions$.pipe(
      ofType<ShowNotificationProductAdded>(AppActionsTypes.ShowNotificationProductAdded),
      tap(o => this.notifyProductService.productAdded(o.warehouseEntry))  
    );

    @Effect({dispatch : false})
    ShowNotificationProductExpired$ = this.actions$.pipe(
      ofType<ShowNotificationProductExpired>(AppActionsTypes.ShowNotificationProductExpired),
      tap(o => this.notifyProductService.produtExpired(o.warehouseEntry))  
    );

    @Effect({dispatch : false})
    ShowNotificationProductRemoved$ = this.actions$.pipe(
      ofType<ShowNotificationProductRemoved>(AppActionsTypes.ShowNotificationProductRemoved),
      tap(o => this.notifyProductService.productRemoved(o.warehouseEntry))  
    );
  }
  