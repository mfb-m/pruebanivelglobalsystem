import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';
import { UserActions, UserActionsTypes } from '../actions/user.actions';
import { ServerActions } from '../actions/server.actions';
import { AppActions, AppActionsTypes } from '../actions/app.actions';
import { ResponseBase } from 'src/app/models/ResponseBase';


const warehouse = new Array<IWarehouseLedgerEntry>()
const initialState = new ResponseBase<IWarehouseLedgerEntry[]>()
initialState.response = warehouse;

export function reducerWarehouseLedgerEntry(
    state: ResponseBase<IWarehouseLedgerEntry[]> = initialState,
    action: ServerActions | AppActions | UserActions
) : ResponseBase<IWarehouseLedgerEntry[]>  {

    switch (action.type) {
        case AppActionsTypes.AddRowToWarehouseLedgerEntry:
            state.response.push(action.entry)
            return {...state}

        case UserActionsTypes.FetchGetWarehouseLedgerEntrySuccess:
            
            return {...action.entries}
        default:
            return state;
    }
}