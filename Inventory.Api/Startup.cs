﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Inventory.Infrastructure.Bus.Commands;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Infrastucture.EntityFramework;
using Inventory.Infrastucture.EntityFramework.Repositories;
using Inventory.Services.Inventory.BackgroundProcess;
using Inventory.Services.Inventory.Commands.AddProduct;
using Inventory.Services.Inventory.Notifications;
using Inventory.Services.Inventory.Repositories;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace Inventory.Api
{
    public class Startup
    {
        private readonly ILoggerFactory _loggerFactory;

        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            _loggerFactory = loggerFactory;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<IMediator, Mediator>();
            services.AddScoped<ServiceFactory>(p => p.GetService);

            services.AddScoped<ICommandBus, CommandBus>();
            services.AddScoped<IQueryBus, QueryBus>();


            services.Scan(scan => scan
                .FromAssembliesOf(typeof(AddProductCommandHandler))
                .AddClasses()
                .AsImplementedInterfaces());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<AddProductValidator>());
            services.AddDbContext<InventoryDbContext>(opt =>
            {
                opt.UseInMemoryDatabase("Inventory");
            });

            services.AddScoped<IInventoryRepository, InventoryRepository>();
            //services.AddHostedService<ExpiredPoductHostedService>();
            //services.AddScoped<IExpiredProductProcess, ExpiredProductProcess>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Inventory API", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddCors(action =>
              action.AddPolicy("cors", builder =>
                builder
                 .AllowAnyMethod()
                 .AllowAnyHeader()
                .WithOrigins("http://localhost:4200", "http://localhost:8080")
                 .AllowCredentials()));
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Inventory API V1");
            });

            app.UseCors("cors");


            app.UseSignalR(route =>
            {
                route.MapHub<InventoryHub>("/inventory");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
