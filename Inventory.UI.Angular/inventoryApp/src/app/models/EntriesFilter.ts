import { EntryType } from './WarehouseLedgerEntry';

export interface IEntriesFilter {
    warehouseId : string
    dateStartEntry? : Date,
    dateEndEntry? : Date,
    entryType?: EntryType[],
    productId? : string
}