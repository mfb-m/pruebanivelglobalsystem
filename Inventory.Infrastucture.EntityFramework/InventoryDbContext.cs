﻿using Inventory.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Inventory.Infrastucture.EntityFramework
{
    public class InventoryDbContext : DbContext
    {
        public InventoryDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Warehouse> Warehouse { get; set; }
        public DbSet<WarehouseLedgerEntry> WarehouseLedgerEntry { get; set; }
        public DbSet<Product> Product { get; set; }
    }
}
