﻿using Inventory.Domain.Entities;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Inventory.Repositories;
using Inventory.Services.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter
{
    public class GetEntriesOfWarehouseByFilterQueryHandler : IQueryHandler<GetEntriesOfWarehouseByFilterQuery, ResponseBase<IEnumerable<WarehouseLedgerEntry>>>
    {
        private readonly IInventoryRepository _inventoryRepository;

        public GetEntriesOfWarehouseByFilterQueryHandler(IInventoryRepository inventoryRepository)
        {
            _inventoryRepository = inventoryRepository;
        }

        public Task<ResponseBase<IEnumerable<WarehouseLedgerEntry>>> Handle(GetEntriesOfWarehouseByFilterQuery request, CancellationToken cancellationToken)
        {
            var entries = _inventoryRepository.GetEntriesOfWarehouseByFilter(request.EntriesFilter, request.PageSize, request.PageNumber);
            var response = new ResponseBase<IEnumerable<WarehouseLedgerEntry>>()
            {
                Count = entries.Count(),
                Errors = null,
                IsValid = true,
                Response = entries,
                Total = _inventoryRepository.GetTotalWarehouseLedgerEntries()
            };
            return Task.FromResult(response);
        }
    }
}
