﻿using Inventory.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Services.Inventory.Notifications
{
    public interface IInventoryNotifications
    {
        Task EntryAddedToLedgerEntry(WarehouseLedgerEntry warehouseLedgerEntry);
    }
}
