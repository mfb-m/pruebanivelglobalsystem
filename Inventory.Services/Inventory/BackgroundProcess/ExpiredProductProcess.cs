﻿using Inventory.Domain.Entities;
using Inventory.Services.Inventory.Notifications;
using Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter;
using Inventory.Services.Inventory.Repositories;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inventory.Services.Inventory.BackgroundProcess
{
    public class ExpiredProductProcess : IExpiredProductProcess
    {
        public ExpiredProductProcess(
            IInventoryRepository inventoryRepository,
            IHubContext<InventoryHub, IInventoryNotifications> hubContext,
            ILogger<ExpiredProductProcess> logger
            )
        {
            _inventoryRepository = inventoryRepository;
            _hubContext = hubContext;
            _logger = logger;
        }

        private readonly IInventoryRepository _inventoryRepository;
        private readonly IHubContext<InventoryHub, IInventoryNotifications> _hubContext;
        private readonly ILogger<ExpiredProductProcess> _logger;

        public void Execute()
        {
            _logger.LogInformation("ExpiredProductProcess START!");
            var products = _inventoryRepository.GetProductsExpired();
            foreach (var product in products.ToList())
            {
                var  entriesForProducts = _inventoryRepository.GetEntriesOfWarehouseByFilter(new EntriesFilter
                {
                    ProductId = product.Id,
                });

                if(_inventoryRepository.GetStockOfProduct(product.Id) > 0)
                {
                    var entry = new WarehouseLedgerEntry
                        {
                            PostingDate = DateTimeOffset.Now,
                            EntryType = EntryType.Depletion,
                            Id = Guid.NewGuid(),
                            Product = product,
                            ProductId = product.Id,
                            Quantity = entriesForProducts.Sum(o => o.Quantity),
                        };
                    _inventoryRepository.RegisterIntLedgerEntry(entry);
                    _inventoryRepository.SaveChanges();
                    _hubContext.Clients.All.EntryAddedToLedgerEntry(entry);
                    _logger.LogInformation($"{product.Name} Expired and removed");
                }
            }
            _logger.LogInformation("ExpiredProductProcess END!");
        }

    }
}
