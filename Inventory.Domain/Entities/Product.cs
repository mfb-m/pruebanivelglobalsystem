﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Domain.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal PurchasePrice { get; set; }
        public DateTimeOffset DateAdded { get; set; }
        public DateTimeOffset? ExpireDate { get; set; }
        public TypeProduct Type { get; set; }
        public decimal Weight { get; set; }

        public static Product CreateProduct(
            Guid Id, 
            string name, 
            string description, 
            decimal salesPrice, 
            decimal purchasePrice, 
            DateTimeOffset DateAdded,
            TypeProduct type,
            decimal weight,
            DateTimeOffset? expireDate = null
            )
        {
            var product = new Product
            {
                Id = Id,
                Name = name,
                Description = description,
                SalesPrice = salesPrice,
                PurchasePrice = purchasePrice,
                ExpireDate = expireDate,
                DateAdded = DateAdded,
                Type = type
            };
            return product;
        }
    }

    public enum TypeProduct
    {
        Perishable, //perecedero
        Preserve, //conserva
        Electronics, // electronica
        Furniture // Muebles
    }
}
