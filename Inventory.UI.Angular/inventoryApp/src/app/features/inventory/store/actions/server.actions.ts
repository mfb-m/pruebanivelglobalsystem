import { Action } from '@ngrx/store';
import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';

export enum ServerActionsTypes {
    ServerSendsWarehouseLedgerEntry = '[ServerSendsWarehouseLedgerEntry] ServerSendsWarehouseLedgerEntry',
}

export class ServerSendsWarehouseLedgerEntry implements Action
{
    readonly type =  ServerActionsTypes.ServerSendsWarehouseLedgerEntry
    constructor(public warehouseEntry : IWarehouseLedgerEntry){} 
}

export type ServerActions = ServerSendsWarehouseLedgerEntry