import { ActionReducerMap } from '@ngrx/store';
import { InventoryState, inventoryReducer } from './features/inventory/store/state/inventory.state';

export interface AppState {
    inventory : InventoryState
  }
  

  export const rootReducer = {
    inventory : inventoryReducer,
  };

  