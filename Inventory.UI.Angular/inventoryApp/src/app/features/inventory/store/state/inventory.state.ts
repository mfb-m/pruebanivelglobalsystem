import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';
import { IWarehouse } from 'src/app/models/Warehouse';
import { ActionReducerMap, combineReducers } from '@ngrx/store';
import { reducerWarehouseLedgerEntry } from '../reducers/warehouse-ledger-entry.reducer';
import { reducerWarehouse } from '../reducers/warehouse.reducer';
import { ResponseBase } from 'src/app/models/ResponseBase';

export interface InventoryState {
    warehouseEntries: ResponseBase<IWarehouseLedgerEntry[]>;
    warehouse : IWarehouse[]
  }
  

  export const inventoryReducer: ActionReducerMap<InventoryState> = {
    warehouseEntries: reducerWarehouseLedgerEntry,
    warehouse : reducerWarehouse
  };
