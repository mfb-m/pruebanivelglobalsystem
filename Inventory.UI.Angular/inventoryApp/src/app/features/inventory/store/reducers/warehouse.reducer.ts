import { UserActions, UserActionsTypes, FetchGetWarehousesSuccess } from '../actions/user.actions';
import { IWarehouse } from 'src/app/models/Warehouse';



const initialState = new Array<IWarehouse>()

export function reducerWarehouse(
    state: IWarehouse[] = initialState,
    action: FetchGetWarehousesSuccess
) : IWarehouse[]  {

    switch (action.type) {
        case UserActionsTypes.FetchGetWarehousesSuccess:
            return Object.assign(state, action.warehouses)
        default:
            return state;
    }
}