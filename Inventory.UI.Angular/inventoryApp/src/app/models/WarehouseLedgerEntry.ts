import { IWarehouse } from './Warehouse';
import { IProduct } from './Product';

export interface IWarehouseLedgerEntry{
    id : string,
    postingDate : Date,
    entryType : EntryType,
    warehouseId : string,
    warehouse : IWarehouse,
    productId: string,
    product : IProduct
    quantity : number
}

export enum EntryType
{
    Purchase, //compra
    Sale, // venta
    Transfer, //tranferir entre inventarios
    Depletion, //caducidad / merma
    PossitiveAdjustment, //ajuste positivo
    NegativeAdjustment // ajuste negativo
}