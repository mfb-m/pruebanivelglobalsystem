import { EntryType } from './WarehouseLedgerEntry';
import { TypeProduct } from './Product';

export interface IAddProductCommand{
    entryId : string,
    dateAdded : Date,
    entryType : EntryType,
    warehouseId : string,
    quantity : number,
    productId : string,
    name : string,
    description : string,
    imageUrl : string,
    salesPrice : number,
    purchasePrice : number,
    expireDate : Date,
    type : TypeProduct,
    weight : number,
}
