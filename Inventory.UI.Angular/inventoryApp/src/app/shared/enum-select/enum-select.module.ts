import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnumPipe } from './pipes/enum.pipe';
import { EntrySelectComponent } from './components/entry-select/entry-select.component';
import { ProductTypeSelectComponent } from './components/product-type-select/product-type-select.component';


@NgModule({
  declarations: [EnumPipe, EntrySelectComponent, ProductTypeSelectComponent],
  imports: [
    CommonModule,
  ],
  exports:[EnumPipe, EntrySelectComponent, ProductTypeSelectComponent]
})
export class EnumSelectModule { }
