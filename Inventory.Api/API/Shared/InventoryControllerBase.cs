﻿using Inventory.Infrastructure.Bus.Commands;
using Inventory.Infrastructure.Bus.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Api.API.Shared
{
    public class InventoryControllerBase : ControllerBase
    {
        protected readonly IQueryBus _queryBus;
        protected readonly ICommandBus _commandBus;

        public InventoryControllerBase(
            IQueryBus queryBus, 
            ICommandBus commandBus
            )
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }
    }
}
