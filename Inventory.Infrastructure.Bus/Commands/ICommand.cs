﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Infrastructure.Bus.Commands
{
    public interface ICommand : IRequest { }
}
