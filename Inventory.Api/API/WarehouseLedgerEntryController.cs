﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Api.API.Shared;
using Inventory.Infrastructure.Bus.Commands;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Inventory.Commands.AddProduct;
using Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Api.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseLedgerEntryController : InventoryControllerBase
    {
        public WarehouseLedgerEntryController(IQueryBus queryBus, ICommandBus commandBus) : base(queryBus, commandBus)
        {
        }

        // GET: api/WarehouseLedgerEntry
        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] EntriesFilter entriesFilter, int pageSize, int pageNumber)
        {
            var response = await _queryBus.Send(new GetEntriesOfWarehouseByFilterQuery
            {
                EntriesFilter = entriesFilter,
                PageNumber = pageNumber,
                PageSize = pageSize
            });
            return Ok(response);
        }
    }
}
