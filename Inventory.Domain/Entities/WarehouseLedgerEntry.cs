﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Domain.Entities
{
    public class WarehouseLedgerEntry
    {
        public Guid Id { get; set; }
        public DateTimeOffset PostingDate { get; set; }
        public EntryType EntryType { get; set; }
        public Guid? WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }

        public static WarehouseLedgerEntry Register(
            Guid id,
            DateTimeOffset postingDate,
            EntryType entryType,
            Guid warehouseId,
            Guid productId,
            int quantity,
            Warehouse warehouse,
            Product product
            )
        {
            var entry = new WarehouseLedgerEntry
            {
                Id = id,
                EntryType = entryType,
                PostingDate = postingDate,
                ProductId = productId,
                Quantity = quantity,
                WarehouseId = warehouseId,
                Warehouse = warehouse,
                Product = product
            };
            return entry;
        }
    }

    public enum EntryType
    {
        Purchase, //compra
        Sale, // venta
        Transfer, //tranferir entre inventarios
        Depletion, //caducidad / merma
        PossitiveAdjustment, //ajuste positivo
        NegativeAdjustment // ajuste negativo
    }

   
}
