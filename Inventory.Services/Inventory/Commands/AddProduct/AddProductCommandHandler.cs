﻿using Inventory.Domain.Entities;
using Inventory.Infrastructure.Bus.Commands;
using Inventory.Services.Inventory.Notifications;
using Inventory.Services.Inventory.Repositories;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inventory.Services.Inventory.Commands.AddProduct
{
    public class AddProductCommandHandler : ICommandHandler<AddProductCommand>
    {
        private readonly IInventoryRepository _inventoryRepository;
        private readonly IHubContext<InventoryHub, IInventoryNotifications> _hubContext;
        private readonly ILogger<AddProductCommand> logger;

        public AddProductCommandHandler(
            IInventoryRepository inventoryRepository, 
            IHubContext<InventoryHub, IInventoryNotifications> hubContext, 
            ILogger<AddProductCommand> logger)
        {
            _inventoryRepository = inventoryRepository;
            _hubContext = hubContext;
            this.logger = logger;
        }

        public Task<Unit> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            var product = Product.CreateProduct(
                request.ProductId,
                request.Name,
                request.Description,
                request.SalesPrice,
                request.PurchasePrice,
                request.DateAdded,
                request.Type,
                request.Weight,
                request.ExpireDate
                );

            var warehouse = _inventoryRepository.GetWarehousesById(request.WarehouseId);
            if(warehouse == null)
                throw new Exception("ERROR Cant find warehouse");

            var entry = WarehouseLedgerEntry.Register(
                request.EntryId,
                request.DateAdded,
                request.EntryType,
                warehouse.Id,
                product.Id,
                request.Quantity,
                warehouse,
                product
                );

            _inventoryRepository.RegisterIntLedgerEntry(entry);

            if (_inventoryRepository.SaveChanges())
                _hubContext.Clients.All.EntryAddedToLedgerEntry(entry);
            else
                throw new Exception("ERROR Cant AddProductCommand");

            return Task.FromResult(new Unit());
        }
    }
}
