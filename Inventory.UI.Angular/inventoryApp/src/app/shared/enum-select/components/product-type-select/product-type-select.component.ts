import { Component, OnInit } from '@angular/core';
import { TypeProduct } from 'src/app/models/Product';

@Component({
  selector: 'app-product-type-select',
  templateUrl: './product-type-select.component.html',
  styleUrls: ['./product-type-select.component.scss']
})
export class ProductTypeSelectComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  entryTypes = TypeProduct;
}
