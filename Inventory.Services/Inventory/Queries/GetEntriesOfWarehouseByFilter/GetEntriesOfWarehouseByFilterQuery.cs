﻿using Inventory.Domain.Entities;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter
{
    public class GetEntriesOfWarehouseByFilterQuery : IQuery<ResponseBase<IEnumerable<WarehouseLedgerEntry>>>
    {
        public EntriesFilter EntriesFilter { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}
