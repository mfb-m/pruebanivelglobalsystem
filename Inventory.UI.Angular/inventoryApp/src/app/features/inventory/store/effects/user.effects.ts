import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { InventoryService } from '../../services/inventory.service';
import { switchMap, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { FetchGetWarehouseLedgerEntry, UserActionsTypes, FetchGetWarehouses, FetchGetWarehousesSuccess, FetchGetWarehousesFail, FetchGetWarehouseLedgerEntryFail, FetchGetWarehouseLedgerEntrySuccess, FetchAddProduct, FetchAddProductSuccess, FetchAddProductFail } from '../actions/user.actions';

@Injectable()
export class UserEffects {

    //Effects
    constructor(private actions$: Actions, private service : InventoryService ) { }


    @Effect()
    fetchGetWarehouseLedgerEntry$ = this.actions$.pipe(
      ofType<FetchGetWarehouseLedgerEntry>(UserActionsTypes.FetchGetWarehouseLedgerEntry),
      switchMap((o) => {
        return this.service.getEntriesOfWarehouseByFilterQuery(o.pageSize, o.pageNumber)
      }),
      switchMap((o)=> [
          new FetchGetWarehouseLedgerEntrySuccess(o)
      ]),
      catchError((error) => {
          return of(new FetchGetWarehouseLedgerEntryFail(error))
      })

    );

    @Effect()
    fetchGetWarehouses$ = this.actions$.pipe(
      ofType<FetchGetWarehouses>(UserActionsTypes.FetchGetWarehouses),
      switchMap(() => {
        return this.service.getWarehouses()
      }),
      switchMap((o)=> of(new FetchGetWarehousesSuccess(o.response))),
      catchError((error) => {
          return of(new FetchGetWarehousesFail(error))
      })

    );

    @Effect()
    FetchAddProduct$ = this.actions$.pipe(
      ofType<FetchAddProduct>(UserActionsTypes.FetchAddProduct),
      switchMap((o) => {
        return this.service.addProduct(o.entry)
      }),
      switchMap((o)=> of(new FetchAddProductSuccess())),
      catchError((error) => {
          return of(new FetchAddProductFail(error))
      })

    );

}
