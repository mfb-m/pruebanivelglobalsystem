import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IAddProductCommand } from 'src/app/models/AddProductCommand';
import { environment } from 'src/environments/environment.prod';
import { IRemoveProductCommand } from 'src/app/models/RemoveProductCommand';
import { IEntriesFilter } from 'src/app/models/EntriesFilter';
import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';
import { ResponseBase } from 'src/app/models/ResponseBase';
import { IWarehouse } from 'src/app/models/Warehouse';

@Injectable()
export class InventoryService {


  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl;
  addProduct(product: IAddProductCommand) {
    return this.http.post(`${this.apiUrl}/product`, product)
  }
  removeProduct(product: IRemoveProductCommand) {
    return this.http.request('delete', `${this.apiUrl}/product`, { body: product })
  }

  getEntriesOfWarehouseByFilterQuery(pageSize: number, pageNumber: number, entriesFilter?: IEntriesFilter) {
    let params = new HttpParams();
    params = params.append('pageSize', pageSize.toString());
    params = params.append('pageNumber', pageNumber.toString());
    return this.http.get<ResponseBase<IWarehouseLedgerEntry[]>>(`${this.apiUrl}/warehouseLedgerEntry`, { params: params })
  }

  getWarehouses() {
    return this.http.get<ResponseBase<IWarehouse[]>>(`${this.apiUrl}/warehouse`)
  }
}
