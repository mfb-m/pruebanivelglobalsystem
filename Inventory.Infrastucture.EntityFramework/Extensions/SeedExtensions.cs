﻿using Bogus;
using Inventory.Domain.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.Infrastucture.EntityFramework.Extensions
{
    public static class SeedExtensions
    {
        public static IWebHost Seed(this IWebHost webHost)
        {
            var scope = webHost.Services.CreateScope();
            var client = scope.ServiceProvider.GetService<InventoryDbContext>();
            var logger = scope.ServiceProvider.GetService<ILogger<InventoryDbContext>>();

            logger.LogInformation("Seed data start");
            var warehouses = new Faker<Warehouse>()
                .RuleFor(o => o.Id, (f, e) => Guid.NewGuid())
                .RuleFor(o => o.Name, (f, e) => f.Vehicle.Model())
                .Generate(3);

            var products = new Faker<Product>()
                .RuleFor(o => o.Id, (f, e) => Guid.NewGuid())
                .RuleFor(o => o.Name, (f, e) => f.Commerce.Product())
                .RuleFor(o => o.Description, (f, e) => f.Commerce.Department())
                .RuleFor(o => o.DateAdded, (f, e) => f.Date.PastOffset())
                .RuleFor(o => o.ExpireDate, (f, e) => f.Date.FutureOffset())
                .RuleFor(o => o.ImageUrl, (f, e) => "https://baconmockup.com/50/50")
                .RuleFor(o => o.PurchasePrice, (f, e) => decimal.Parse(f.Commerce.Price()))
                .RuleFor(o => o.SalesPrice, (f, e) => decimal.Parse(f.Commerce.Price()))
                .RuleFor(o => o.Type, (f, e) => e.Type = TypeProduct.Preserve)
                .RuleFor(o => o.Weight, (f, e) => f.Random.Decimal())
                .Generate(1000);

            

            var warehouseLedgerEntry = new Faker<WarehouseLedgerEntry>()
                .RuleFor(o => o.Id, (f, e) => Guid.NewGuid())
                .RuleFor(o => o.EntryType, (f, e) => e.EntryType = EntryType.Purchase)
                .RuleFor(o => o.PostingDate, (f, e) => f.Date.PastOffset())
                .RuleFor(o => o.ProductId, (f, e) => f.PickRandom(products).Id)
                .RuleFor(o => o.Quantity, (f, e) => f.Random.Int(1, 100))
                .RuleFor(o => o.WarehouseId, (f, e) => f.PickRandom(warehouses).Id)
                .Generate(1000);

            warehouseLedgerEntry.ForEach(o =>
            {
                o.Product = products.FirstOrDefault(p => p.Id == o.ProductId);
                o.Warehouse = warehouses.FirstOrDefault(p => p.Id == o.WarehouseId);
            });

            client.Warehouse.Add(new Warehouse { Id = new Guid("441e9f22-c867-4ada-a186-387c6836c8b0"), Name = "Test" });

            client.Product.AddRange(products);
            client.Warehouse.AddRange(warehouses);
            client.WarehouseLedgerEntry.AddRange(warehouseLedgerEntry);
            client.SaveChanges();

            logger.LogInformation("Seed data ended");
            return webHost;
        }

    }
}
