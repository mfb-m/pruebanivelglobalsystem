﻿using Inventory.Domain.Entities;
using Inventory.Infrastucture.EntityFramework.Extensions;
using Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter;
using Inventory.Services.Inventory.Repositories;
using Inventory.Services.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.Infrastucture.EntityFramework.Repositories
{
    public class InventoryRepository : RepositoryBase, IInventoryRepository
    {
        private readonly InventoryDbContext _inventoryDbContext;

        public InventoryRepository(InventoryDbContext inventoryDbContext) : base(inventoryDbContext)
        {
            _inventoryDbContext = inventoryDbContext;
        }

        public IEnumerable<WarehouseLedgerEntry> GetEntriesOfWarehouseByFilter(EntriesFilter entriesFilter, int pageSize, int pageNumber)
        {
            var query = _inventoryDbContext.WarehouseLedgerEntry.Include(o => o.Product).Include(o => o.Warehouse).AsQueryable();

            if (entriesFilter.DateEndEntry != null && entriesFilter.DateStartEntry != null)
                query = query.Where(p => p.PostingDate >= entriesFilter.DateStartEntry)
                             .Where(p => p.PostingDate <= entriesFilter.DateEndEntry);

            if (entriesFilter.EntryType != null)
                foreach (var entryType in entriesFilter.EntryType)
                {
                    query = query.Where(p => p.EntryType == entryType);
                }

            if (entriesFilter.ProductId != null)
                query = query.Where(p => p.ProductId == entriesFilter.ProductId);

            if (entriesFilter.WarehouseId != null)
                query = query.Where(p => p.WarehouseId == entriesFilter.WarehouseId);

            return query.OrderByDescending(o => o.PostingDate).GetPaged(pageNumber, pageSize);
        }

        public Product GetProductById(Guid productId)
        {
            return _inventoryDbContext.Product.FirstOrDefault(o => o.Id == productId);
        }

        //public IEnumerable<Product> GetProductsByFilter(ProductFilter productFilter, int pageSize, int pageNumber)
        //{
        //    var query = _inventoryDbContext.Product.AsQueryable();

        //    if (productFilter.DateAddedEnd != null && productFilter.DateAddedStart != null)
        //        query.Where(p => p.DateAdded >= productFilter.DateAddedStart)
        //             .Where(p => p.DateAdded <= productFilter.DateAddedEnd);

        //    if (productFilter.ExpireDateEnd != null && productFilter.ExpireDateStart != null)
        //        query.Where(p => p.ExpireDate >= productFilter.ExpireDateStart)
        //             .Where(p => p.ExpireDate <= productFilter.ExpireDateEnd);

        //    if (!string.IsNullOrEmpty(productFilter.Name))
        //        query.Where(p => p.Name.Contains(productFilter.Name, StringComparison.CurrentCultureIgnoreCase));

        //    if (productFilter.PurchasePrice != null)
        //        query.Where(p => p.PurchasePrice == productFilter.PurchasePrice);

        //    if (productFilter.SalesPrice != null)
        //        query.Where(p => p.SalesPrice == productFilter.SalesPrice);

        //    if (productFilter.Type != null)
        //        query.Where(p => p.Type == productFilter.Type);

        //    if (productFilter.Weight != null)
        //        query.Where(p => p.Weight == productFilter.Weight);
            

        //    return query.GetPaged(pageNumber, pageSize);

        //}

        public IEnumerable<Product> GetProductsExpired()
        {
            return _inventoryDbContext.Product.Where(o => o.ExpireDate <= DateTimeOffset.Now);
        }

        public int GetStockOfProduct(Guid productId)
        {
            var negatives = _inventoryDbContext.WarehouseLedgerEntry.Where(o => o.ProductId == productId)
                .Where(o => o.EntryType == EntryType.NegativeAdjustment || o.EntryType == EntryType.Sale || o.EntryType == EntryType.Depletion)
                .Sum(o => o.Quantity);

            var positives = _inventoryDbContext.WarehouseLedgerEntry.Where(o => o.ProductId == productId)
                .Where(o => o.EntryType == EntryType.Purchase || o.EntryType == EntryType.PossitiveAdjustment)
                .Sum(o => o.Quantity);

            return positives - negatives;
        }

        public int GetTotalWarehouseLedgerEntries()
        {
            return _inventoryDbContext.WarehouseLedgerEntry.Count();
        }

        public IEnumerable<Warehouse> GetWarehouses()
        {
            return _inventoryDbContext.Warehouse.ToList();
        }



        public Warehouse GetWarehousesById(Guid warehouseId)
        {
            return _inventoryDbContext.Warehouse.FirstOrDefault(o => o.Id == warehouseId);
        }

        public void RegisterIntLedgerEntry(WarehouseLedgerEntry productLedgerEntry)
        {
            _inventoryDbContext.WarehouseLedgerEntry.Add(productLedgerEntry);
        }
    }
}
