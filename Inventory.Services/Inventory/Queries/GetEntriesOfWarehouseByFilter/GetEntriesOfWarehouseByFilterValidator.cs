﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter
{
    public class GetEntriesOfWarehouseByFilterValidator : AbstractValidator<GetEntriesOfWarehouseByFilterQuery>
    {
        public GetEntriesOfWarehouseByFilterValidator()
        {
            RuleFor(o => o.PageNumber).NotNull().NotEmpty();
            RuleFor(o => o.PageSize).NotNull().NotEmpty();
        }
    }
}
