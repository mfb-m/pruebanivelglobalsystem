﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Commands.AddProduct
{
    public class AddProductValidator : AbstractValidator<AddProductCommand>
    {
        public AddProductValidator()
        {
            RuleFor(o => o.DateAdded).NotEmpty().NotNull();
            RuleFor(o => o.Description).NotEmpty().NotNull();
            RuleFor(o => o.EntryId).NotEmpty().NotNull();
            RuleFor(o => o.ExpireDate).NotEmpty().NotNull();
            RuleFor(o => o.ImageUrl).NotEmpty().NotNull();
            RuleFor(o => o.Name).NotEmpty().NotNull();
            RuleFor(o => o.ProductId).NotEmpty().NotNull();
            RuleFor(o => o.PurchasePrice).NotEmpty().NotNull();
            RuleFor(o => o.Quantity).NotEmpty().NotNull();
            RuleFor(o => o.SalesPrice).NotEmpty().NotNull();
            RuleFor(o => o.Type).NotNull();
            RuleFor(o => o.Quantity).NotEmpty().NotNull();
            RuleFor(o => o.WarehouseId).NotEmpty().NotNull();
            RuleFor(o => o.Weight).NotEmpty().NotNull();
        }
    }
}
