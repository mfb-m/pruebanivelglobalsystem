import { Injectable } from '@angular/core';
import { SnotifyService, SnotifyToastConfig, SnotifyPosition } from 'ng-snotify';
import { IProduct } from 'src/app/models/Product';
import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';

@Injectable({
  providedIn: 'root'
})
export class NotifyProductService {

  constructor(private snotifyService: SnotifyService) { }

  getConfig(): SnotifyToastConfig {
    return {
      position: SnotifyPosition.rightTop,
      timeout: 8000
    };
  }

  productAdded(warehouseLedgerEntry : IWarehouseLedgerEntry){
    this.snotifyService.success(`${warehouseLedgerEntry.product.name}, quantity ${warehouseLedgerEntry.quantity}`, "Product ADDED!", {
      ...this.getConfig(),
      icon: warehouseLedgerEntry.product.imageUrl,
    });
  }
  
  productRemoved(warehouseLedgerEntry : IWarehouseLedgerEntry){

    this.snotifyService.error(`${warehouseLedgerEntry.product.name}, quantity ${warehouseLedgerEntry.quantity}`, "Product REMOVED!", {
      ...this.getConfig(),
      icon: warehouseLedgerEntry.product.imageUrl,
    });
  }

  produtExpired(warehouseLedgerEntry : IWarehouseLedgerEntry){
    this.snotifyService.warning(`${warehouseLedgerEntry.product.name}, quantity ${warehouseLedgerEntry.quantity}`, "Product EXPIRED!", {
      ...this.getConfig(),
      icon: warehouseLedgerEntry.product.imageUrl,
    });
  }
}
