import { Component, OnInit } from '@angular/core';
import { NotifyProductService } from 'src/app/shared/notify-product/services/notify-product.service';
import { EntryType } from 'src/app/models/WarehouseLedgerEntry';
import { TypeProduct } from 'src/app/models/Product';
import { InventoryWebshocketsService } from '../../services/inventory-webshockets.service';
import { Store } from '@ngrx/store';
import { ConnectWebshockets } from '../../store/actions/app.actions';
import { InventoryState } from '../../store/state/inventory.state';

@Component({
  selector: 'app-inventory-page',
  templateUrl: './inventory-page.component.html',
  styleUrls: ['./inventory-page.component.scss']
})
export class InventoryPageComponent implements OnInit {

  constructor(private store : Store<InventoryState>, private inventoryWebshocketsService : InventoryWebshocketsService) {

   }

  ngOnInit() {
    this.store.dispatch(new ConnectWebshockets());
    this.inventoryWebshocketsService.listenServerEvents()
  }

}
