import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { IWarehouseLedgerEntry, EntryType } from 'src/app/models/WarehouseLedgerEntry';
import { FetchGetWarehouseLedgerEntry } from '../../store/actions/user.actions';
import { map, tap } from 'rxjs/operators';
import { InventoryState } from '../../store/state/inventory.state';
import { AppState } from 'src/app/app.state';

@Component({
  selector: 'app-warehouse-ledger-entry-table',
  templateUrl: './warehouse-ledger-entry-table.component.html',
  styleUrls: ['./warehouse-ledger-entry-table.component.scss']
})
export class WarehouseLedgerEntryTableComponent implements OnInit {
  data$: Observable<IWarehouseLedgerEntry[]>;
  currentPage : number = 1
  totalItems : number;
  itemsPerPage : number = 6
  EntryType = EntryType;

  constructor(private store: Store<AppState>) { }



  ngOnInit() {
    this.store.dispatch(new FetchGetWarehouseLedgerEntry(this.itemsPerPage, this.currentPage))
    this.data$ = this.store.select(o => o.inventory.warehouseEntries).pipe(
      tap((o) => {this.totalItems = o.total}),
      map(o => o.response.sort((a: IWarehouseLedgerEntry, b: IWarehouseLedgerEntry) => {
        return new Date(b.postingDate).getTime() - new Date(a.postingDate).getTime();
    }))
    )
  }

  pageChanged(page){
    console.log(page)
    this.currentPage = page
    this.store.dispatch(new FetchGetWarehouseLedgerEntry(this.itemsPerPage, this.currentPage))
  }

  Columns$ = new BehaviorSubject<string[]>([
    'postingDate',
    'entryType',
    'warehouse',
    'product',
    'expireDate',
    'image',
    'quantity',
  ]);

}
