﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.BackgroundProcess
{
    public interface IExpiredProductProcess
    {
        void Execute();
    }
}
