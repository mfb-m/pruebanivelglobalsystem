# Prueba de nivel, por manuel fernandez-montes baños

#Para ejecutar

```sh
$ docker-compose up -d
```

#Para visualizar el frontend
```sh
$ http://localhost:8080
```

#Para visualizar swagger
```sh
$ http://localhost:5000/swagger
```