import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarShellComponent } from 'src/app/shared/shell/components/navbar-shell/navbar-shell.component';
import { InventoryPageComponent } from './pages/inventory-page/inventory-page.component';

const routes: Routes = [
  { path : '', 
  component : NavbarShellComponent,
  children : [
    {path : '' , component : InventoryPageComponent}
  ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
