
export interface IProduct{
    id : string,
    name : string,
    description : string,
    imageUrl : string,
    salesPrice: Number,
    purchasePrice : Number,
    dateAdded : Date,
    expireDate : Date,
    type : TypeProduct,
    weight : number
}


export enum TypeProduct{
    Perishable, //perecedero
    Preserve, //conserva
    Electronics, // electronica
    Furniture // Muebles
}