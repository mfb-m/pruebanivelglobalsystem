import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { ServerSendsWarehouseLedgerEntry, ServerActionsTypes } from '../actions/server.actions';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { tap, switchMap } from 'rxjs/operators';
import { InventoryWebshocketsService } from '../../services/inventory-webshockets.service';
import { IWarehouseLedgerEntry, EntryType } from 'src/app/models/WarehouseLedgerEntry';
import { ShowNotificationProductAdded } from '../actions/app.actions';

@Injectable()
export class ServerEffects {

    //Effects
    constructor(private actions$: Actions, private inventoryWebshocketsService: InventoryWebshocketsService) { }

}
