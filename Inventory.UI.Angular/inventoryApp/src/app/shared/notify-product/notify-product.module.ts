import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnotifyService } from 'ng-snotify';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers :[
    SnotifyService
  ]
})
export class NotifyProductModule { }
