import { EntryType } from './WarehouseLedgerEntry';

export interface IRemoveProductCommand{
    entryId : string,
    dateAdded: Date,
    entryType : EntryType,
    warehouseId : string,
    quantity : number,
    productId : string
}