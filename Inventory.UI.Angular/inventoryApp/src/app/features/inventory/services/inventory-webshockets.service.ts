import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from "@aspnet/signalr";
import { IWarehouseLedgerEntry, EntryType } from 'src/app/models/WarehouseLedgerEntry';
import { NotifyProductService } from 'src/app/shared/notify-product/services/notify-product.service';
import { Store } from '@ngrx/store';
import { ShowNotificationProductRemoved, AddRowToWarehouseLedgerEntry, ShowNotificationProductAdded, ShowNotificationProductExpired } from '../store/actions/app.actions';
import { ServerSendsWarehouseLedgerEntry } from '../store/actions/server.actions';
import { InventoryState } from '../store/state/inventory.state';
import { environment } from 'src/environments/environment.prod';
@Injectable()
export class InventoryWebshocketsService {
  hubConnection: HubConnection;




  constructor(private store: Store<InventoryState>) {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(environment.webshocketsUrl)
      .build();
  }

  initConnection() {
    return this.hubConnection.start();
  }

  listenServerEvents() {
    this.hubConnection.on("EntryAddedToLedgerEntry", (entry: IWarehouseLedgerEntry) => {
      this.store.dispatch(new ServerSendsWarehouseLedgerEntry(entry))
      this.store.dispatch(new AddRowToWarehouseLedgerEntry(entry))
      switch (+entry.entryType) {
        case EntryType.NegativeAdjustment:
          this.store.dispatch(new ShowNotificationProductRemoved(entry))
          break;
        case EntryType.Sale:
          this.store.dispatch(new ShowNotificationProductRemoved(entry))
          break;
        case EntryType.PossitiveAdjustment:
          this.store.dispatch(new ShowNotificationProductAdded(entry))
          break;
        case EntryType.Purchase:
          this.store.dispatch(new ShowNotificationProductAdded(entry))
          break;
        case EntryType.Depletion:
          this.store.dispatch(new ShowNotificationProductExpired(entry))
          break;
        default:
          break;
      }
    })
  }

}
