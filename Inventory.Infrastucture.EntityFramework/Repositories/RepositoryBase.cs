﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Infrastucture.EntityFramework.Repositories
{
    public class RepositoryBase
    {
        private readonly InventoryDbContext _inventoryDbContext;

        public RepositoryBase(InventoryDbContext inventoryDbContext)
        {
            _inventoryDbContext = inventoryDbContext;
        }

        public bool SaveChanges()
        {
            return _inventoryDbContext.SaveChanges() > 0;
        }
    }
}
