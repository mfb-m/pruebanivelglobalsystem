import { Action } from '@ngrx/store';
import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';
import { IWarehouse } from 'src/app/models/Warehouse';
import { ResponseBase } from 'src/app/models/ResponseBase';
import { IAddProductCommand } from 'src/app/models/AddProductCommand';

export enum UserActionsTypes {
    FetchAddProduct = '[api/Product] fetch all themes theme',
    FetchAddProductFail = '[api/Product] get theme success',
    FetchAddProductSuccess = '[api/Product] get theme fail',
    FetchRemoveProduct = '[api/Product] fetch all themes theme',
    FetchRemoveProductFail = '[api/Product] get theme success',
    FetchRemoveProductSuccess = '[api/Product] get theme fail',
    FetchGetWarehouseLedgerEntry = '[api/WarehouseLedgerEntry] WarehouseLedgerEntry',
    FetchGetWarehouseLedgerEntrySuccess = '[api/WarehouseLedgerEntry] WarehouseLedgerEntry success',
    FetchGetWarehouseLedgerEntryFail = '[api/WarehouseLedgerEntry] WarehouseLedgerEntry fail',
    FetchGetWarehouses = '[api/Warehouse] FetchGetWarehouses',
    FetchGetWarehousesSuccess = '[api/Warehouse] FetchGetWarehousesSuccess',
    FetchGetWarehousesFail = '[api/Warehouse] FetchGetWarehousesFail',
}

export class FetchGetWarehouseLedgerEntrySuccess implements Action
{
    readonly type =  UserActionsTypes.FetchGetWarehouseLedgerEntrySuccess
    constructor(public entries : ResponseBase<IWarehouseLedgerEntry[]>){} 
}

export class FetchGetWarehouseLedgerEntryFail implements Action
{
    readonly type =  UserActionsTypes.FetchGetWarehouseLedgerEntryFail
    constructor(public errors : string[]){} 
}

export class FetchGetWarehouseLedgerEntry implements Action
{
    readonly type =  UserActionsTypes.FetchGetWarehouseLedgerEntry
    constructor(public pageSize: number, public pageNumber : number){} 
}

export class FetchGetWarehouses implements Action
{
    readonly type =  UserActionsTypes.FetchGetWarehouses
    constructor(){} 
}

export class FetchGetWarehousesFail implements Action
{
    readonly type =  UserActionsTypes.FetchGetWarehousesFail
    constructor(public errors : string[]){} 
}

export class FetchGetWarehousesSuccess implements Action
{
    readonly type =  UserActionsTypes.FetchGetWarehousesSuccess
    constructor(public warehouses: IWarehouse[]){} 
}

export class FetchAddProduct implements Action
{
    readonly type =  UserActionsTypes.FetchAddProduct
    constructor(public entry: IAddProductCommand){} 
}

export class FetchAddProductSuccess implements Action
{
    readonly type =  UserActionsTypes.FetchAddProductSuccess
    constructor(){} 
}

export class FetchAddProductFail implements Action
{
    readonly type =  UserActionsTypes.FetchAddProductFail
    constructor(public errors: string[]){} 
}

export type UserActions = 
FetchGetWarehouseLedgerEntrySuccess |
FetchGetWarehouseLedgerEntryFail |
FetchGetWarehouseLedgerEntry |
FetchGetWarehousesSuccess |
FetchGetWarehousesFail |
FetchGetWarehouses |
FetchAddProduct |
FetchAddProductSuccess |
FetchAddProductFail
