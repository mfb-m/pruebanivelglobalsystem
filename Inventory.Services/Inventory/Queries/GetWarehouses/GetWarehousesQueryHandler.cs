﻿using Inventory.Domain.Entities;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Inventory.Repositories;
using Inventory.Services.Shared;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inventory.Services.Inventory.Queries.GetWarehouses
{
    public class GetWarehousesQueryHandler : IQueryHandler<GetWarehousesQuery, ResponseBase<IEnumerable<Warehouse>>>
    {
        private readonly IInventoryRepository _inventoryRepository;

        public GetWarehousesQueryHandler(IInventoryRepository inventoryRepository)
        {
            _inventoryRepository = inventoryRepository;
        }

        public Task<ResponseBase<IEnumerable<Warehouse>>> Handle(GetWarehousesQuery request, CancellationToken cancellationToken)
        {
            var warehouses = _inventoryRepository.GetWarehouses();
            var response = new ResponseBase<IEnumerable<Warehouse>>
            {
                Count = warehouses.Count(),
                Errors = null,
                IsValid = true,
                Response = warehouses
            };
            return Task.FromResult(response);
        }
    }
}
