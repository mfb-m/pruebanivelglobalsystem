﻿using Inventory.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter
{
    public class EntriesFilter
    {
        public Guid? WarehouseId { get; set; }
        public DateTimeOffset? DateStartEntry { get; set; }
        public DateTimeOffset? DateEndEntry { get; set; }
        public EntryType?[] EntryType { get; set; }
        public Guid? ProductId { get; set; }
    }
}
