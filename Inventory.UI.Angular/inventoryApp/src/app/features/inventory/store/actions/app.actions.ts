import { Action } from '@ngrx/store';
import { IWarehouseLedgerEntry } from 'src/app/models/WarehouseLedgerEntry';

export enum AppActionsTypes {
    ConnectWebshockets = '[APP WEBSHOCKETS] ConnectWebshockets',
    ConnectWebshocketsFail = '[APP WEBSHOCKETS] ConnectWebshocketsFail',
    ConnectWebshocketsSucess = '[APP WEBSHOCKETS] ConnectWebshocketsSucess',
    ShowNotificationProductAdded = '[APP NOTIFICATION] SendNotificationProductAdded',
    ShowNotificationProductRemoved = '[APP NOTIFICATION] SendNotificationProductRemoved',
    ShowNotificationProductExpired = '[APP NOTIFICATION] SendNotificationProductExpired',
    ShowModal = '[APP ShowModal] ShowModal',
    AddRowToWarehouseLedgerEntry = '[APP AddRowToWarehouseLedgerEntry] AddRowToWarehouseLedgerEntry',
}

export class ConnectWebshockets implements Action
{
    readonly type =  AppActionsTypes.ConnectWebshockets
    constructor(){} 
}

export class ConnectWebshocketsFail implements Action
{
    readonly type =  AppActionsTypes.ConnectWebshocketsFail
    constructor(public error : any){} 
}


export class ConnectWebshocketsSucess implements Action
{
    readonly type =  AppActionsTypes.ConnectWebshocketsSucess
    constructor(){} 
}


export class ShowNotificationProductAdded implements Action
{
    readonly type =  AppActionsTypes.ShowNotificationProductAdded
    constructor(public warehouseEntry : IWarehouseLedgerEntry){} 
}

export class ShowNotificationProductExpired implements Action
{
    readonly type =  AppActionsTypes.ShowNotificationProductExpired
    constructor(public warehouseEntry : IWarehouseLedgerEntry){} 
}

export class ShowNotificationProductRemoved implements Action
{
    readonly type =  AppActionsTypes.ShowNotificationProductRemoved
    constructor(public warehouseEntry : IWarehouseLedgerEntry){} 
}


export class ShowModal implements Action
{
    readonly type =  AppActionsTypes.ShowModal
    constructor(){} 
}

export class AddRowToWarehouseLedgerEntry implements Action
{
    readonly type =  AppActionsTypes.AddRowToWarehouseLedgerEntry
    constructor(public entry : IWarehouseLedgerEntry){} 
}

export type AppActions = 
ConnectWebshockets |
ConnectWebshocketsSucess |
ConnectWebshocketsFail |
ShowNotificationProductAdded |
ShowNotificationProductRemoved |
ShowNotificationProductExpired |
ShowModal |
AddRowToWarehouseLedgerEntry