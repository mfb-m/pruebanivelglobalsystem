import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryPageComponent } from './pages/inventory-page/inventory-page.component';
import { WarehouseLedgerEntryTableComponent } from './components/warehouse-ledger-entry-table/warehouse-ledger-entry-table.component';
import { ShellModule } from 'src/app/shared/shell/shell.module';
import { CdkTableModule } from '@angular/cdk/table';
import { NotifyProductModule } from 'src/app/shared/notify-product/notify-product.module';
import { InventoryWebshocketsService } from './services/inventory-webshockets.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './store/effects/app.effects';
import { UserEffects } from './store/effects/user.effects';
import { ServerEffects } from './store/effects/server.effects';
import { InventoryService } from './services/inventory.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { EnumSelectModule } from 'src/app/shared/enum-select/enum-select.module';
import { AddProductComponent } from './components/add-product/add-product.component';
import { inventoryReducer } from './store/state/inventory.state';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AddProductComponent, InventoryPageComponent, WarehouseLedgerEntryTableComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('inventory', inventoryReducer),
    EffectsModule.forFeature([AppEffects, UserEffects, ServerEffects]),
    InventoryRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ShellModule,
    CdkTableModule,
    NotifyProductModule,
    NgxPaginationModule,
    EnumSelectModule
  ],
  providers : [InventoryWebshocketsService, InventoryService]
})
export class InventoryModule { }
