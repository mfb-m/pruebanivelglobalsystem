﻿using Inventory.Domain.Entities;
using Inventory.Infrastructure.Bus.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Commands.AddProduct
{
    public class AddProductCommand : ICommand
    {
        public Guid EntryId { get; set; }
        public DateTimeOffset DateAdded { get; set; }
        public EntryType EntryType { get; set; }
        public Guid WarehouseId { get; set; }
        public int Quantity { get; set; }
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal PurchasePrice { get; set; }
        public DateTimeOffset? ExpireDate { get; set; }
        public TypeProduct Type { get; set; }
        public decimal Weight { get; set; }
    }
}
