﻿using Inventory.Domain.Entities;
using Inventory.Services.Inventory.Queries.GetEntriesOfWarehouseByFilter;
using Inventory.Services.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Services.Inventory.Repositories
{
    public interface IInventoryRepository : IRepositoryBase
    {
        void RegisterIntLedgerEntry(WarehouseLedgerEntry productLedgerEntry);
        IEnumerable<Warehouse> GetWarehouses();
        int GetTotalWarehouseLedgerEntries();
        Warehouse GetWarehousesById(Guid warehouseId);
        Product GetProductById(Guid productId);
        IEnumerable<Product> GetProductsExpired();
        int GetStockOfProduct(Guid productId);
        IEnumerable<WarehouseLedgerEntry> GetEntriesOfWarehouseByFilter(EntriesFilter entriesFilter , int pageSize = 100, int pageNumber = 1);
    }
}
