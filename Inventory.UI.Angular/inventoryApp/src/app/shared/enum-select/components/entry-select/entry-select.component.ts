import { Component, OnInit } from '@angular/core';
import { EntryType } from 'src/app/models/WarehouseLedgerEntry';
import * as M from 'materialize-css/dist/js/materialize';

@Component({
  selector: 'app-entry-select',
  templateUrl: './entry-select.component.html',
  styleUrls: ['./entry-select.component.scss'],
})
export class EntrySelectComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  entryTypes = EntryType;

}
