﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Api.API.Shared;
using Inventory.Infrastructure.Bus.Commands;
using Inventory.Infrastructure.Bus.Queries;
using Inventory.Services.Inventory.Queries.GetWarehouses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inventory.Api.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseController : InventoryControllerBase
    {

        public WarehouseController(IQueryBus queryBus, ICommandBus commandBus) : base(queryBus, commandBus)
        {
        }
        // GET: api/Warehouse
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var response = await _queryBus.Send(new GetWarehousesQuery());
            return Ok(response);
        }
    }
}
